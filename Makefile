# Makefile per articolo in LaTeX

default: all

all:
	#make clean
	pdflatex final_talk.tex
	pdflatex final_talk.tex
	#dvips -t landscape final_talk.dvi -o
	#ps2pdf final_talk.ps
		
.tex:
	latex $@
	dvips -t a4 $?.dvi -o
	ps2pdf $@.ps
	
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.class
	rm -f *.bcf
	rm -f *.out
	rm -f *.nav
	rm -f *.snm
	rm -f *.run.xml
	rm -f *~

erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera il file .pdf di tutti i file .tex"
	@echo "make talk: genera il file .pdf " 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e il file .pdf"
	@echo "Leggere anche README"
