\documentclass{beamer}
\usetheme[color=green,titlepagelogo=figures/unict_logo.png,assistantsupervisor=true]{TorinoTh}
\author{Dott. Fabio Rinnone}
\rel{\vspace{0.5em}Prof. Alfredo Pulvirenti} %% workaround issue #3
\assistantsupervisor{Prof.ssa Rosalba Giugno}
\title{NetMatch*: una app Cytoscape per la ricerca\\
di sottostrutture in reti biologiche}
\ateneo{Università degli Studi di Catania}
\date{Corso di Laurea Magistrale in Informatica, 29 settembre 2017}

\usepackage[italian]{babel}
\usepackage{graphicx,multicol,tikz,subfigure,caption,adjustbox}

\usetikzlibrary{shapes,arrows}

\DeclareCaptionLabelSeparator{horse}{:\hspace{1ex}} 
	\captionsetup{
		labelsep = horse,
		figureposition = bottom
} 

\setbeamertemplate{caption}[numbered]

\theoremstyle{plain}              
\newtheorem{teo}{Teorema}[section]
\newtheorem{prop}[teo]{Proposizione}
\newtheorem{cor}[teo]{Corollario}
\newtheorem{lem}[teo]{Lemma}
\theoremstyle{definition}
\newtheorem{defin}{Definizione}
\newtheorem{ese}{Esempio}
%\theoremstyle{remark}
\newtheorem{oss}{Osservazione}

\newcommand{\mysize}{\fontsize{3pt}{6pt}\selectfont}

\begin{document}
\titlepageframe

\begin{tframe}{Introduzione}

\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
di una rete query all'interno di una rete target e di verificare la sua significatività rispetto 
a sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=8cm]{figures/netmatch.png}
\end{center}
\end{figure}

\end{tframe}

\begin{tframe}{Subgraph matching}

\vspace{0.5cm}

Il problema del subgraph matching è \textbf{NP-Completo}.

\vspace{0.5cm}

\begin{defin}
Un grafo $G=(V,E)$ è un sottografo isomorfo di $G'=(V',E')$ se esiste $f:V\rightarrow V'$ tale che
$(u,v)\in E$ se e solo se $(f(u),f(v))\in E'$. In altre parole è possibile rietichettare i vertici
di $G$ con i vertici di $G'$, mantenendo i corrispondenti archi in $G$.
\end{defin}

\end{tframe}

\begin{tframe}{Subgraph matching}

\vspace{0.5cm}

\begin{figure}[H]
\centering
\subfigure[]{%
        \begin{tikzpicture}[scale = 1, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
        \tikzstyle{node_style} = [circle, draw = black, fill = white, semithick]
        \tikzstyle{edge_style} = [draw = black, semithick]
        \node[node_style] (v1) at (-2,2) {1};
        \node[node_style] (v2) at (-2,0) {2};
        \draw[edge_style] (v1) edge (v2);
        \end{tikzpicture}}
\qquad\qquad\qquad
\subfigure[]{%
        \begin{tikzpicture}[scale = 1, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
        \tikzstyle{node_style} = [circle, draw = black, fill = white, semithick]
        \tikzstyle{edge_style} = [draw = black, semithick]
        \node[node_style] (v1) at (-2,2) {1};
        \node[node_style] (v2) at (-2,0) {2};
        \node[node_style] (v3) at (0,0) {3};
        \draw[edge_style] (v1) edge (v2);
        \draw[edge_style] (v2) edge (v3);
        \draw[edge_style] (v1) edge (v3);
        \end{tikzpicture}}
\caption{Grafo query $G_q$ (a) e grafo target $G_t$ (b).}
\end{figure}

\end{tframe}

\begin{tframe}{Subgraph matching}

\vspace{0.8cm}

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[node distance=1cm]
\tikzstyle{node_style_normal} = [rectangle, draw=white, minimum width=1cm, minimum height=0.5cm,
	draw=white, text centered, font=\footnotesize]
\tikzstyle{node_style_bold} = [text = black, font=\footnotesize\bfseries]
\tikzstyle{edge_style_normal} = [draw = black, semithick]
\tikzstyle{edge_style_bold} = [draw = black, very thick]

\node (root) at (0,0) [node_style_bold] {root};
\node (n1) at (-4,-1) [node_style_bold] {(1,1)};
\node (n2) at (0,-1)[node_style_normal] {(1,2)};
\node (n3) at (4,-1) [node_style_normal] {(1,3)};
\node (n4) at (-5,-2) [node_style_bold] {(2,2)};
\node (n5) at (-3,-2) [node_style_normal] {(2,3)};
\node (n6) at (-1,-2) [node_style_normal] {(2,1)};
\node (n7) at (1,-2) [node_style_normal] {(2,3)};
\node (n8) at (3,-2) [node_style_normal] {(2,2)};
\node (n9) at (5,-2) [node_style_normal] {(2,3)};

\draw [edge_style_bold] (root) edge (n1);
\draw [edge_style_normal] (root) edge (n2);
\draw [edge_style_normal] (root) edge (n3);
\draw [edge_style_bold] (n1) edge (n4);
\draw [edge_style_normal] (n1) edge (n5);
\draw [edge_style_normal] (n2) edge (n6);
\draw [edge_style_normal] (n2) edge (n7);
\draw [edge_style_normal] (n3) edge (n8);
\draw [edge_style_normal] (n3) edge (n9);
\end{tikzpicture}
\end{center}
\caption{Albero di ricerca per la risoluzione del problema del subgraph matching.}
\end{figure}

\end{tframe}

\begin{tframe}{Algoritmo RI}

L'efficienza di un algoritmo dipende da:

\begin{itemize}

\item euristica adottata per attraversare l'albero di ricerca

\item vincoli applicati prima e durante il suo attraversamento

\end{itemize}

L'algoritmo RI:

\begin{itemize}

\item effettua un ordinamento dei vertici di $G_q$ indipendentemente da $G_t$
(GreatestConstraintFirst)

\item individua una strategia di ricerca ottimale

\item riduce lo spazio di ricerca applicando opportune condizioni di isomorfismo

\end{itemize}

\end{tframe}

\begin{tframe}{Opzioni}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{multicols}{3}
\centering
\includegraphics[width=2.75cm]{figures/matchingPanel.png}\quad
\includegraphics[width=2.75cm]{figures/significancePanel.png}\quad
\includegraphics[width=2.75cm]{figures/libraryPanel.png}\quad
\end{multicols}
\caption{Schede del pannello principale di NetMatch*.}
\end{figure}

\end{tframe}

\begin{tframe}{Query approssimate}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=7cm]{figures/approxQuery.png}
\end{center}
\caption{Query approssimate in NetMatch*.}
\end{figure}

\end{tframe}

\begin{tframe}{Output}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/results6.png}
\end{center}
\caption{Output in NetMatch*.}
\end{figure}

\end{tframe}

\begin{tframe}{Motifs}

\vspace{-1.2em}

\begin{figure}[H]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.315, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, font=\scriptsize]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (3,0) {?};
	\node[node_style] (v3) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.315, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, font=\scriptsize]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (-3,0) {?};
	\node[node_style] (v3) at (3,0) {?};
	\node[node_style] (v4) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v2) edge (v4);
	\draw[edge_style] (v3) edge (v4);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.315, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, font=\scriptsize]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (3,0) {?};
	\node[node_style] (v3) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v1) edge (v3);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale=0.315, shorten >=1pt, auto, node distance=3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, font=\scriptsize]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (-3,2) {?};
	\node[node_style] (v2) at (3,2) {?};
	\node[node_style] (v3) at (-3,-2) {?};
	\node[node_style] (v4) at (3,-2) {?};
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v2) edge (v4);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale=0.315, shorten >=1pt, auto, node distance=3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, font=\scriptsize]
	\tikzstyle{empty_style} = [draw = white, fill = white, font=\scriptsize]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[empty_style] at (0,2) {\ldots m \ldots};
	\node[empty_style] at (0,-2) {\ldots n \ldots};
	\node[node_style] (v1) at (-3,2) {?};
	\node[node_style] (v2) at (3,2) {?};
	\node[node_style] (v3) at (-3,-2) {?};
	\node[node_style] (v4) at (3,-2) {?};
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v2) edge (v4);
 	\end{tikzpicture}}
\caption
{Query predefinite di NetMatch*: three-chain (a); bi-parallel (b); feed-forward loop (c);
bi-fan (d); m-to-n-fan (e).}
\end{figure}

\end{tframe}

\begin{tframe}{Significatività}

\begin{tabular}{p{.4\textwidth} p{.6\textwidth}} 
\raggedright

{\scriptsize{

\vspace{0.25cm}

Modelli di randomizzazione:

\begin{itemize}

\item Shuffling

\item Erd\H{o}s-R\'{e}nyi

\item Watts-Strogatz

\item Barab\'{a}si-Albert

\item Geometrico

\item Forest-fire

\item Duplicazione

\end{itemize}

Misure:

\begin{itemize}

\item $Z$-score

\item $e$-value

\end{itemize}

\vspace{\baselineskip}}} &

\vspace{0.5cm}

\adjincludegraphics[width=4.5cm,valign=t]{figures/wattsStrogatz}

\end{tabular}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/chartsPPI-4.png}
\end{center}
\caption{Tempi di esecuzione su reti di interazione proteina-proteina.}
\end{figure}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/chartProteinBackbones.png}
\caption{Tempi di esecuzione su proteine 3D.}
\end{center}
\end{figure}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/chartProteinContactMaps.png}
\end{center}
\caption{Tempi di esecuzione su mappe di contatto di proteine.}
\end{figure}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\vspace{-0.5cm}
\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/chart_9ldb.png}
\end{center}
\caption{Tempi di esecuzione di RI e RI-DS.}
\end{figure}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\vspace{0.5cm}
\begin{figure}[H]
\begin{center}
\includegraphics[width=9cm]{figures/feedForwardLoop.png}
\end{center}
\caption{Tempi di esecuzione di query approssimate con feed-forward loop.}
\end{figure}

\end{tframe}

\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

\begin{figure}[H]
\begin{center}
\includegraphics[width=8cm]{figures/models.png}
\end{center}
\caption{Tempi di esecuzione della generazione e ricerca nelle reti random.}
\end{figure}

\end{tframe}

%\begin{tframe}{Risultati}

%\footnotesize{\textbf{NetMatch*} è un'app Cytoscape che consente di ricercare tutte le occorrenze 
%di una rete query all'interno di una rete target e di verificare la sua significatività come motivo 
%rispetto sette modelli di randomizzazione.}

%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=8cm]{figures/zScore.png}
%\end{center}
%\caption{Valori dello Z-score per il feed-forward loop su \emph{Mus musculus}.}
%\end{figure}

%\end{tframe}

\begin{tframe}{Bibliografia}

\begin{thebibliography}{10}

\setbeamertemplate{bibliography item}[article]

\vspace{0.5cm}

\bibitem{RMB+15}
Rinnone~F., Micale~G., Bonnici~V. \emph{et al.}
\newblock \href{https://f1000research.com/articles/4-479/v2}
{NetMatchStar: an enhanced Cytoscape network querying app [version 2; referees: 2 approved].}
\newblock F1000Research. 2015.

\vspace{0.25cm}

\bibitem{BGP+13}
Bonnici~V., Giugno~R., Pulvirenti~A. \emph{et al.}
\newblock \href{https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-S7-S13}
{A subgraph isomorphism algorithm and its application to biochemical data.}
\newblock BMC Bioinformatics. 2013.

\end{thebibliography}

\end{tframe}

\end{document}
